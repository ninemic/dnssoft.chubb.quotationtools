
let budingDetail = {
    init: function () {
        this.cacheDom();
        this.bindEvents();
    },
    cacheDom: function () {
        this.$el = $("#risk-code");
        this.$modal = $("#riskSearchModal");
        this.$riskList = this.$modal.find("ul");
        this.$searchButton = this.$el.find("button");
    },
    bindEvents: function () {
        this.$modal.on("hide.bs.modal", this.chooseRiskCode.bind(this));
    },
    render: function () {
        var newTr =
            ` <tr>
                <td>123456789</td>
                <td>ความคุมครองความเสียหายจากน้ำท่วม</td>
                <td>50000</td>
                <td>100000</td>
                <td>100</td>
                <td>1,000,000</td>
                <td>50000</td>
                <td>??????????</td>
            </tr>`;

        this.$table.find("tbody").append(newTr);
    },
    addPeril: function () {
        this.render();
    },
    chooseRiskCode: function () {
        this.$riskList.find('li')
    },
}

budingDetail.init();
﻿(function () {
    var step = $('#wizardStep').val() - 1;

    var navListItems = $('ul.setup-panel li');

    navListItems.eq(step).removeClass('disabled').addClass('active');

})();


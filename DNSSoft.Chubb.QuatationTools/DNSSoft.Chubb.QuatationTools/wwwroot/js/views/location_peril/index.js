﻿
let locationPeril = {
    init: function () {
        this.cacheDom();
        this.bindEvents();
    },
    cacheDom: function () {
        this.$el = $("#locationPeril");
        this.$addButton = this.$el.find("button");
        this.$table = this.$el.find("table");
    },
    bindEvents: function () {
        this.$addButton.click(this.addPeril.bind(this));
    },
    render: function () {
        var newTr = 
            ` <tr>
                <td>123456789</td>
                <td>ความคุมครองความเสียหายจากน้ำท่วม</td>
                <td>50000</td>
                <td>100000</td>
                <td>100</td>
                <td>1,000,000</td>
                <td>50000</td>
                <td>??????????</td>
            </tr>`;
        
        this.$table.find("tbody").append(newTr);
    },
    addPeril: function () {
        this.render();
    }
}

//locationPeril.init();
﻿(function () {

    var name = document.getElementById('pieChart');

    var myChart = function (name) {

        if (!name) {
            return;
        }

        new Chart(name, {
            type: 'pie',
            data: {
                labels: ["Pendding", "Inprogress", "Inbox", "Total"],
                datasets: [{
                    label: '# of Votes',
                    data: [20, 1, 30, 51],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: []
                },
                legend: {
                    position: 'right',
                }
            }
        });
    }

    myChart(name);

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function () {
        console.log( " href" , this.href )
        console.log( "url", url)
        return this.href == url 
    }).parent().addClass('active')

    // while (true) {
    //     if (element.is('li')) {
    //         element = element.parent().addClass('in').parent();
    //     } else {
    //         break;
    //     }
    // }

})();



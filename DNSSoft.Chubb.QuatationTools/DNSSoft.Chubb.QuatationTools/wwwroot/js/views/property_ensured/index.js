﻿
let ensured = {
    init: function () {
        this.cacheDom();
        this.bindEvents();
    },
    cacheDom: function () {
        this.$summary = $("#summary span");
        this.$form = $("form");
        this.$dynamicInsured = this.$form.find("#dynamic-ensured");
        this.$addButton = this.$form.find("button");
    },
    bindEvents: function () {
        this.$addButton.click(this.addEnsured.bind(this));
        this.$form.keyup(this.calculateSummary.bind(this));
    },
    render: function () {
        var newdiv = document.createElement('div');
        newdiv.innerHTML =
            `<div id="dynamic-ensured" class="">
                    <div class="form-group col-xs-10 col-md-4">
                        <select id="businessType" class="form-control">
                            <option value="home">ตัวบ้านไม่รวมฐานราก </option>
                            <option value="tenant">ทรัพย์สินภายในบ้าน </option>
                            <option value="tenant">สต๊อกสินค้า </option>
                        </select>
                    </div>
                    <div class="form-group col-xs-10 col-md-3">
                        <input type="number" name="insured-cover" min="0" step="1000" max="10000000" class="form-control" />
                    </div>
                    <div class="clearfix"></div>
                </div>`;
        this.$dynamicInsured.append(newdiv);
    },
    addEnsured: function () {
        this.render();
    },
    calculateSummary: function () {
        let sum = 0;
        this.$form.find("[name='insured-cover']").each(function () {
            if ($(this).val()) {
                sum += parseInt($(this).val());
            }
        })
        this.$summary.text(sum);
    }
}

ensured.init();
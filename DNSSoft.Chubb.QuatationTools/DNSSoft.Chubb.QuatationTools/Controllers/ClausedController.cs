﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DNSSoft.Chubb.QuatationTools.Controllers
{
    public class ClausedController : Controller
    {
        // GET: Calused
        public ActionResult Index()
        {
            return View();
        }

        // GET: Calused/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Calused/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Calused/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Calused/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Calused/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Calused/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Calused/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
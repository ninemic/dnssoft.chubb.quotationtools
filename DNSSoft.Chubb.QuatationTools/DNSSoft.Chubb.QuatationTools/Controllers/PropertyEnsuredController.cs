﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DNSSoft.Chubb.QuatationTools.Controllers
{
    public class PropertyEnsuredController : Controller
    {
        // GET: PropertyEnsured
        public ActionResult Index()
        {
            return View();
        }

        // GET: PropertyEnsured/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PropertyEnsured/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PropertyEnsured/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PropertyEnsured/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PropertyEnsured/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PropertyEnsured/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PropertyEnsured/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}